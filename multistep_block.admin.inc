<?php
/**
 * @file
 * Administrtative functions for multistep blocks
 */

module_load_include('inc', 'block', 'block.admin');

/**
 * Add a multistep block
 */
function multistep_block_add_block_form($form, &$form_state) {
  return block_admin_configure($form, $form_state, 'multistep_block', NULL);
}

/**
 * Save the multistep block to the database
 */
function multistep_block_add_block_form_submit($form, &$form_state) {
  $multistep_block = array(
    'steps' => check_plain($form_state['values']['steps']),
    'pass_arguments' => check_plain($form_state['values']['arguments']),
    );
  drupal_write_record('multistep_block', $multistep_block);
  $mbid = $multistep_block ['mbid'];
  $delta = "multistep_block_$mbid";
  $query = db_insert('block')->fields(array('visibility', 'pages', 'custom', 'title', 'module', 'theme', 'status', 'weight', 'delta', 'cache'));
  foreach (list_themes() as $key => $theme) {
    if ($theme->status) {
      $query->values(array(
        'visibility' => (int) $form_state['values']['visibility'],
        'pages' => trim($form_state['values']['pages']),
        'custom' => (int) $form_state['values']['custom'],
        'title' => $form_state['values']['title'],
        'module' => $form_state['values']['module'],
        'theme' => $theme->name,
        'status' => 0,
        'weight' => 0,
        'delta' => $delta,
        'cache' => DRUPAL_NO_CACHE,
        ));
    }
  }
  $query->execute();
  $query = db_insert('block_role')->fields(array('rid', 'module', 'delta'));
  foreach (array_filter($form_state['values']['roles']) as $rid) {
    $query->values(array(
      'rid' => $rid,
      'module' => $form_state['values']['module'],
      'delta' => $delta,
      ));
  }
  $query->execute();
  // Store regions per theme for this block
  foreach ($form_state['values']['regions'] as $theme => $region) {
    db_merge('block')
    ->key(array('theme' => $theme, 'delta' => $delta, 'module' => $form_state['values']['module']))
    ->fields(array(
      'region' => ($region == BLOCK_REGION_NONE ? '' : $region),
      'pages' => trim($form_state['values']['pages']),
      'status' => (int) ($region != BLOCK_REGION_NONE),
      ))
    ->execute();
  }
  drupal_set_message(t('The block has been created.'));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/block';
}

/**
 * Delete the multistep block
 */
function multistep_block_delete($form, &$form_state, $delta = 0) {
  $matches = array();
  preg_match('/_[0-9]+$/', $delta, $matches);
  $mbid = substr($matches[0], 1);
  $form['info'] = array(
    '#type' => 'hidden',
    '#value' => "Multistep block $mbid",
  );
  $form['mbid'] = array(
    '#type' => 'hidden',
    '#value' => $mbid,
  );
  $message = t('Are you sure you want to delete the block %name?', array('%name' => $form['info']['#value']));
  return confirm_form($form, $message, 'admin/structure/block', $message, t('Delete'), t('Cancel'));
}
/**
 * Form submission handler for block_custom_block_delete().
 *
 * @see block_custom_block_delete()
 */

 
function multistep_block_delete_submit($form, &$form_state) {
  $block_id= $form_state['values']['mbid'];  
  $blockid= "multistep_block_{$form_state['values']['mbid']}"; 
  $block_deleted = db_delete('block')
  ->condition('module', 'multistep_block')
  ->condition('delta', $blockid)
  ->execute();
  $blocks_deleted = db_delete('multistep_block')
  ->condition('mbid', $block_id)
  ->execute();
  drupal_set_message(t('The block %name has been removed.', array('%name' => $form_state['values']['info'])));  
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/block';
  return;
}