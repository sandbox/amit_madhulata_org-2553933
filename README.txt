This module provides a new type of block that can be used to create multistep wizards in Drupal using existing pages, views, forms, etc...

Installation
------------

Download the module from Drupal.org and install it.

With drush:
        drush dl multistep_block
        drush en -y multistep_block

Without drush:
1. Download the module tarball or zip.
2. Decompress the module.
3. Transfer it to your drupal install's sites/all/modules folder via ftp.
4. Navigate to your Drupal's admin/build/modules page.
5. Find the module in the "Other" section.
6. Check it off, scroll to the bottom of the page and click submit.

Usage
-----

1. Navigate to admin/structure/block
2. Click on the "Add multistep block" tab.
3. Put the drupal internal paths to your pages, one line after another, into the steps field.
4. Optionally check off the pass arguments option.
5. Click save and publish your block to where you want it.

Author:
-------
Madhulata Infotech(Team). 
